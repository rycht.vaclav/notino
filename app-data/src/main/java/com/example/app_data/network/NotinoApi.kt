package com.example.app_data.network

import com.example.app_domain.model.Product
import com.example.app_domain.model.ProductResult
import retrofit2.Call
import retrofit2.http.GET

/*
* Created by Václav Rychtecký on 05/06/2023
*/
interface NotinoApi {

    @GET("cernfr1993/notino-assignment/db")
    fun getProducts(): Call<ProductResult>
}