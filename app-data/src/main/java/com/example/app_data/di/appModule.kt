package com.example.app_data.di

import com.example.app_data.feature.favorite.FavoriteRepositoryImpl
import com.example.app_data.feature.favorite.FavoriteSourceImpl
import com.example.app_data.feature.products.ProductsRepositoryImpl
import com.example.app_data.feature.products.ProductsSourceImpl
import com.example.app_data.realm.RealmModule
import com.example.app_domain.feature.favorite.FavoriteRepository
import com.example.app_domain.feature.favorite.FavoriteSource
import com.example.app_domain.feature.favorite.GetFavoriteUseCase
import com.example.app_domain.feature.favorite.GetFavoritesUseCase
import com.example.app_domain.feature.favorite.SaveFavoriteUseCase
import com.example.app_domain.feature.products.FetchProductsUseCase
import com.example.app_domain.feature.products.ProductsRepository
import com.example.app_domain.feature.products.ProductsSource
import org.koin.dsl.module

/*
* Created by Václav Rychtecký on 05/06/2023
*/
val appModule = module {

    single { FetchProductsUseCase(get()) }
    single { SaveFavoriteUseCase(get()) }
    single { GetFavoriteUseCase(get()) }
    single { GetFavoritesUseCase(get()) }

    single<ProductsSource> { ProductsSourceImpl(get()) }
    single<ProductsRepository> { ProductsRepositoryImpl(get()) }
    single<FavoriteRepository> { FavoriteRepositoryImpl(get()) }
    single<FavoriteSource> { FavoriteSourceImpl() }

    single { RealmModule() }
}