package com.example.app_data.feature.products

import com.example.app_data.network.NotinoApi
import com.example.app_domain.Result
import com.example.app_domain.exceptions.EmptyBodyException
import com.example.app_domain.feature.products.ProductsSource
import com.example.app_domain.model.ApiErrorResult
import com.example.app_domain.model.Product
import com.example.app_domain.safeCall
import retrofit2.awaitResponse
import timber.log.Timber

/*
* Created by Václav Rychtecký on 05/07/2023
*/
class ProductsSourceImpl(val notinoApi: NotinoApi) : ProductsSource {
    override suspend fun getProductsFromBackend(): Result<List<Product>> {
        return safeCall(
            call = { fetchProducts() },
            errorMessage = "Cannot fetch products - UnexpectedError"
        )
    }

    private suspend fun fetchProducts(): Result<List<Product>> {
        val response = notinoApi.getProducts().awaitResponse()
        return if (response.isSuccessful) {
            val body = response.body()?.vpProductByIds

            Timber.d("RESULTZ: ${body?.size}")

            if (body != null) {
                Result.Success(data = body)
            } else {
                Result.Error(
                    ApiErrorResult(
                        code = response.code(),
                        errorMessage = response.message(),
                        apiThrowable = EmptyBodyException()
                    )
                )
            }
        } else {
            Result.Error(ApiErrorResult(code = response.code(), errorMessage = response.message()))
        }
    }
}