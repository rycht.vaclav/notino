package com.example.app_data.feature.favorite

import com.example.app_data.feature.favorite.model.FavoriteDao
import com.example.app_domain.model.Favorite
import io.realm.RealmResults

/*
* Created by Václav Rychtecký on 05/08/2023
*/
object FavoriteMapper {
    fun mapFromFavoriteDao(data: RealmResults<FavoriteDao>): List<Favorite> {
        return data.map { Favorite(id = it.productId, isFavorite = it.isFavorite) }
    }
}