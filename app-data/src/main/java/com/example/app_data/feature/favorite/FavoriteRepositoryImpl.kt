package com.example.app_data.feature.favorite

import com.example.app_domain.Result
import com.example.app_domain.feature.favorite.FavoriteRepository
import com.example.app_domain.feature.favorite.FavoriteSource
import com.example.app_domain.model.Favorite

/*
* Created by Václav Rychtecký on 05/08/2023
*/
class FavoriteRepositoryImpl(private val favoriteSource: FavoriteSource): FavoriteRepository {
    override suspend fun saveFavorite(params: Favorite) {
        favoriteSource.saveFavoriteToDB(params)
    }

    override suspend fun getFavorite(params: String): Favorite {
        return favoriteSource.getFavoriteFromDB(params)
    }

    override suspend fun getFavorites(): Result<List<Favorite>> {
        return favoriteSource.getFavoritesFromDB()
    }
}