package com.example.app_data.feature.products

import com.example.app_domain.Result
import com.example.app_domain.feature.products.ProductsRepository
import com.example.app_domain.feature.products.ProductsSource
import com.example.app_domain.model.Product

/*
* Created by Václav Rychtecký on 05/07/2023
*/
class ProductsRepositoryImpl(private val productsSource: ProductsSource) : ProductsRepository {
    override suspend fun fetchProduct(): Result<List<Product>> {
        return productsSource.getProductsFromBackend()
    }
}