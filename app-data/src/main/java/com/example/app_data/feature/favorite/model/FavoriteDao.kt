package com.example.app_data.feature.favorite.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/*
* Created by Václav Rychtecký on 05/08/2023
*/
@RealmClass
open class FavoriteDao(
    @PrimaryKey
    var productId: String = "",
    var isFavorite: Boolean = false
) : RealmObject()