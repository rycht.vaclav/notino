package com.example.app_data.feature.favorite

import com.example.app_data.feature.favorite.model.FavoriteDao
import com.example.app_domain.Result
import com.example.app_domain.feature.favorite.FavoriteSource
import com.example.app_domain.model.Favorite
import io.realm.Realm

/*
* Created by Václav Rychtecký on 05/08/2023
*/
class FavoriteSourceImpl : FavoriteSource {
    override suspend fun saveFavoriteToDB(params: Favorite) {
        val realmAccess = Realm.getDefaultInstance()
        return realmAccess.use { realm ->
            val favorite =
                realmAccess.where(FavoriteDao::class.java).equalTo("productId", params.id)
                    .findFirst()
            favorite?.let {
                realm.executeTransaction {
                    it.copyToRealmOrUpdate(
                        FavoriteDao(
                            productId = params.id,
                            isFavorite = params.isFavorite
                        )
                    )
                }
            } ?: run {
                realm.executeTransaction {
                    val newFavorite = realm.createObject(FavoriteDao::class.java, params.id)
                    newFavorite.isFavorite = params.isFavorite
                }
            }
        }
    }

    override suspend fun getFavoriteFromDB(params: String): Favorite {
        val favorite =
            Realm.getDefaultInstance().where(FavoriteDao::class.java).equalTo("productId", params)
                .findFirst()
        return favorite?.let {
            Favorite(
                id = favorite.productId,
                isFavorite = favorite.isFavorite
            )

        } ?: Favorite(id = params, isFavorite = false)
    }

    override fun getFavoritesFromDB(): Result<List<Favorite>> {
        Realm.getDefaultInstance().use {
            return Result.Success(
                FavoriteMapper.mapFromFavoriteDao(
                    it.where(FavoriteDao::class.java).findAll()
                )
            )
        }
    }
}