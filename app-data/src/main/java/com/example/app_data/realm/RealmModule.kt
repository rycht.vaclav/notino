package com.example.app_data.realm

import com.example.app_data.feature.favorite.model.FavoriteDao
import io.realm.annotations.RealmModule

/*
* Created by Václav Rychtecký on 05/06/2023
*/
@RealmModule(classes = [FavoriteDao::class])
open class RealmModule