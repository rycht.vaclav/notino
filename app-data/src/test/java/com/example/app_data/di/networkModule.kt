package com.example.app_data.di

import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/*
* Created by Václav Rychtecký on 05/06/2023
*/
val networkModule = module {
    factory { LoggingInterceptor() }
    factory { provideOkHttpClient(get()) }
    factory { provideKyttenApi(get()) }
    single { provideRetrofit(get()) }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl("https://my-json-server.typicode.com/").client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(loggingInterceptor: LoggingInterceptor): OkHttpClient {
    return OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).build()
}

fun provideKyttenApi(retrofit: Retrofit): NotinoApi = retrofit.create(NotinoApi::class.java)