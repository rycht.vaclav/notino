package com.example.app_domain.model

/*
* Created by Václav Rychtecký on 05/06/2023
*/
data class Product(
    val productId: String,
    val brand: Brand,
    val attributes: Attributes,
    val annotation: String,
    val masterId: Int,
    val url: String,
    val orderUnit: String,
    val price: Price,
    val imageUrl: String,
    val name: String,
    val productCode: String,
    val reviewSummary: ReviewSummary,
    val stockAvailability: StockAvailability,
    var isFavorite: Boolean = false,
    var isInCase: Boolean = false
)