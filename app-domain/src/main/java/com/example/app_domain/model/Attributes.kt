package com.example.app_domain.model

/*
* Created by Václav Rychtecký on 05/06/2023
*/
data class Attributes(
    val master: Boolean,
    val airTransportDisallowed: Boolean,
    val packageSize: PackageSize,
    val freeDelivery: Boolean
)