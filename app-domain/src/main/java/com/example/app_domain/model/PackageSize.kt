package com.example.app_domain.model

/*
* Created by Václav Rychtecký on 05/06/2023
*/
data class PackageSize(
    val height: Int,
    val width: Int,
    val depth: Int
)