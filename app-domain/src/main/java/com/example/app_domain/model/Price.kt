package com.example.app_domain.model

/*
* Created by Václav Rychtecký on 05/06/2023
*/
data class Price(
    val value: Int,
    val currency: String
)