package com.example.app_domain.model

/*
* Created by Václav Rychtecký on 05/07/2023
*/
data class ProductResult(
    val vpProductByIds: List<Product>
)