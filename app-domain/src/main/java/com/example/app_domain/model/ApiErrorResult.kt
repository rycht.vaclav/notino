package com.example.app_domain.model

import com.example.app_domain.ErrorResult

/*
* Created by Václav Rychtecký on 05/07/2023
*/
data class ApiErrorResult(
    val code: Int,
    val errorMessage: String? = null,
    val apiThrowable: Throwable? = null
) : ErrorResult(errorMessage, apiThrowable)