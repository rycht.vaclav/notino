package com.example.app_domain.model

/*
* Created by Václav Rychtecký on 05/08/2023
*/
data class Favorite(
    val id: String,
    val isFavorite: Boolean
)