package com.example.app_domain.model

/*
* Created by Václav Rychtecký on 05/06/2023
*/
data class StockAvailability(
    val code: String,
    val count: String
)