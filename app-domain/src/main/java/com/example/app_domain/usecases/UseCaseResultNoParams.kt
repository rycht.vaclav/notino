package com.example.app_domain.usecases

import com.example.app_domain.Result
import com.example.app_domain.model.UseCase

/*
* Created by Václav Rychtecký on 05/06/2023
*/
abstract class UseCaseResultNoParams<out T : Any> : UseCase<Result<T>, Unit>() {
    suspend operator fun invoke() = super.invoke(Unit)
}