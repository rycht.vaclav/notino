package com.example.app_domain.feature.favorite

import com.example.app_domain.Result
import com.example.app_domain.model.Favorite

/*
* Created by Václav Rychtecký on 05/08/2023
*/
interface FavoriteSource {
    suspend fun saveFavoriteToDB(params: Favorite)
    suspend fun getFavoriteFromDB(params: String): Favorite
    fun getFavoritesFromDB(): Result<List<Favorite>>
}