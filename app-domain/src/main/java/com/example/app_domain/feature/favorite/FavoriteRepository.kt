package com.example.app_domain.feature.favorite

import com.example.app_domain.Result
import com.example.app_domain.model.Favorite

/*
* Created by Václav Rychtecký on 05/08/2023
*/
interface FavoriteRepository {
    suspend fun saveFavorite(params: Favorite)
    suspend fun getFavorite(params: String): Favorite
    suspend fun getFavorites(): Result<List<Favorite>>
}