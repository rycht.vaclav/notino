package com.example.app_domain.feature.products

import com.example.app_domain.Result
import com.example.app_domain.model.Product

/*
* Created by Václav Rychtecký on 05/07/2023
*/
interface ProductsRepository {
    suspend fun fetchProduct(): Result<List<Product>>
}