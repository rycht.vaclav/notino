package com.example.app_domain.feature.favorite

import com.example.app_domain.model.Favorite
import com.example.app_domain.model.UseCase

/*
* Created by Václav Rychtecký on 05/08/2023
*/
class GetFavoriteUseCase(private val favoriteRepository: FavoriteRepository) :
    UseCase<Favorite, String>() {

    override suspend fun doWork(params: String): Favorite = favoriteRepository.getFavorite(params)
}