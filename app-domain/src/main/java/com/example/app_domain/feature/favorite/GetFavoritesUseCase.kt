package com.example.app_domain.feature.favorite

import com.example.app_domain.Result
import com.example.app_domain.model.Favorite
import com.example.app_domain.usecases.UseCaseResultNoParams

/*
* Created by Václav Rychtecký on 05/08/2023
*/
class GetFavoritesUseCase(private val favoriteRepository: FavoriteRepository): UseCaseResultNoParams<List<Favorite>>() {
    override suspend fun doWork(params: Unit): Result<List<Favorite>> = favoriteRepository.getFavorites()
}