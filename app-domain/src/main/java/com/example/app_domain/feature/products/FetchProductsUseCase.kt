package com.example.app_domain.feature.products

import com.example.app_domain.model.Product
import com.example.app_domain.usecases.UseCaseResultNoParams

/*
* Created by Václav Rychtecký on 05/07/2023
*/
class FetchProductsUseCase(private val productsRepository: ProductsRepository) :
    UseCaseResultNoParams<List<Product>>() {
    override suspend fun doWork(params: Unit) = productsRepository.fetchProduct()
}