package com.example.app_domain.exceptions

import java.io.IOException

/*
* Created by Václav Rychtecký on 05/07/2023
*/
class EmptyBodyException : IOException("Response with empty body")