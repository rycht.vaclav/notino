package com.rychtecky.notino.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app_domain.Result
import com.example.app_domain.feature.favorite.GetFavoriteUseCase
import com.example.app_domain.feature.favorite.GetFavoritesUseCase
import com.example.app_domain.feature.favorite.SaveFavoriteUseCase
import com.example.app_domain.feature.products.FetchProductsUseCase
import com.example.app_domain.model.Favorite
import com.example.app_domain.model.Product
import com.rychtecky.notino.Constants
import com.rychtecky.notino.Extensions.updateCaseStatus
import com.rychtecky.notino.Extensions.updateFavoriteStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import timber.log.Timber

/*
* Created by Václav Rychtecký on 05/06/2023
*/
class NotinoViewModel(
    private val fetchProductsUseCase: FetchProductsUseCase,
    private val saveFavoriteUseCase: SaveFavoriteUseCase,
    private val getFavoriteUseCase: GetFavoriteUseCase,
    private val getFavoritesUseCase: GetFavoritesUseCase
) : ViewModel() {

    private val _productList = MutableStateFlow<List<Product>?>(null)
    val productList = _productList

    fun fetchProductsAndMarkAsFavorite() {
        viewModelScope.launch(Dispatchers.IO) {
            var result: Result<List<Product>>? = null

            while (result?.isSuccess() != true) {
                result = fetchProductsUseCase()
                if (result.isError()) {
                    val error = result.errorOrNull()
                    Timber.d(error?.throwable, "Product isError. Data %s", error!!.message)
                }
                delay(Constants.RETRY_TIMEOUT)
            }
            markFavoriteProducts(result.getOrNull())
        }
    }

    fun saveFavoriteAndMark(product: Product) {
        viewModelScope.launch(Dispatchers.IO) {
            Timber.d("PRODUCT: ${product.productId}")
            saveFavoriteUseCase(Favorite(id = product.productId, isFavorite = !product.isFavorite))
            markProductAsFavorite(product.productId)
        }
    }

    private suspend fun markProductAsFavorite(productId: String) {
        val favorite = getFavoriteUseCase(productId)
        _productList.value = productList.updateFavoriteStatus(favorite.id, favorite.isFavorite)
    }

    private suspend fun markFavoriteProducts(products: List<Product>?) {
        viewModelScope.launch(Dispatchers.IO) {
            products?.let { products ->
                val favorites = getFavoritesUseCase().getOrNull()
                favorites?.let {
                    products.forEach { product ->
                        product.isFavorite = favorites.find { it.id == product.productId }?.isFavorite ?: false
                    }
                }
                products.forEach {
                    Timber.d("IS FAVORITE: ${it.isFavorite}")
                }
                _productList.value = products
            }
        }
    }

    fun changeInCaseStatus(productId: String) {
        _productList.value = productList.updateCaseStatus(productId)
    }
}