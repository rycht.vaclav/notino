package com.rychtecky.notino

import android.app.Application
import com.example.app_data.di.appModule
import com.example.app_data.network.networkModule
import com.example.app_data.realm.RealmModule
import com.rychtecky.notino.di.viewModelModule
import io.realm.Realm
import io.realm.RealmConfiguration
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.component.KoinComponent
import org.koin.core.context.startKoin
import timber.log.Timber

/*
* Created by Václav Rychtecký on 05/06/2023
*/
class NotinoApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        initKoin()

        Realm.init(this)

        val realmModule = getKoinInstance<RealmModule>()
        val realmConfig = RealmConfiguration.Builder()
            .name("notino.realm")
            .modules(realmModule)
            .deleteRealmIfMigrationNeeded()
            .schemaVersion(1)
            .build()

        Realm.setDefaultConfiguration(realmConfig)
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@NotinoApplication)
            modules(
                viewModelModule + appModule + networkModule
            )
        }
    }

    inline fun <reified T : Any> getKoinInstance(): T {
        return object : KoinComponent {
            val value: T by inject()
        }.value
    }
}