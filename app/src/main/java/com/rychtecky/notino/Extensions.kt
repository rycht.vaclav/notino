package com.rychtecky.notino

import com.example.app_domain.model.Product
import kotlinx.coroutines.flow.MutableStateFlow

/*
* Created by Václav Rychtecký on 05/08/2023
*/
object Extensions {
    fun MutableStateFlow<List<Product>?>.updateCaseStatus(id: String): List<Product>? {
        return this.value?.let {
            it.map { product ->
                if (product.productId == id) {
                    product.copy(isInCase = !product.isInCase)
                } else {
                    product
                }
            }
        }
    }

    fun MutableStateFlow<List<Product>?>.updateFavoriteStatus(
        id: String,
        favorite: Boolean
    ): List<Product>? {
        return this.value?.let {
            it.map { product ->
                if (product.productId == id) {
                    product.copy(isFavorite = favorite)
                } else {
                    product
                }
            }
        }
    }

    fun String.toCzk(): String {
        return when (this) {
            "CZK" -> "Kč"
            else -> "Unknown currency"
        }
    }
}