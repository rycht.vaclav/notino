package com.rychtecky.notino.activites

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import org.koin.androidx.viewmodel.ext.android.viewModel
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.rychtecky.notino.ui.screen.MainScreen
import com.rychtecky.notino.ui.theme.NotinoTheme
import com.rychtecky.notino.viewmodels.NotinoViewModel

class MainActivity : ComponentActivity() {

    private val viewModel: NotinoViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        hideStatusBar()

        viewModel.fetchProductsAndMarkAsFavorite()

        setContent {
            NotinoTheme {
                MainScreen(viewModel = viewModel)
            }
        }
    }

    private fun hideStatusBar() {
        WindowInsetsControllerCompat(window, window.decorView).apply {
            // Hide the status bar
            hide(WindowInsetsCompat.Type.statusBars())
            // Hide navigation bar
            hide(WindowInsetsCompat.Type.navigationBars())
            // Allow showing the status bar with swiping from top to bottom
            systemBarsBehavior =
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
    }

    override fun onResume() {
        super.onResume()
        hideStatusBar()
    }
}
