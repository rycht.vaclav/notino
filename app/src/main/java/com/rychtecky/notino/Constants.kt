package com.rychtecky.notino

/*
* Created by Václav Rychtecký on 05/06/2023
*/
object Constants {
    const val RETRY_TIMEOUT = 5000L
}