package com.rychtecky.notino.di

import com.rychtecky.notino.viewmodels.NotinoViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

/*
* Created by Václav Rychtecký on 05/06/2023
*/
val viewModelModule: Module = module {
    viewModel { NotinoViewModel(get(), get(), get(), get()) }
}