package com.rychtecky.notino.ui.theme

import androidx.compose.runtime.Immutable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp


/*
* Created by Václav Rychtecký on 05/06/2023
*/
val large500 = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Medium,
    lineHeight = 27.sp,
    fontSize = 21.sp
)
val regular500 = large500.copy(fontSize = 19.sp)
val regular400 = large500.copy(fontSize = 19.sp, fontWeight = FontWeight.Light)

@Immutable
data class TextAppearance(
    val large500: TextStyle,
    val regular500: TextStyle,
    val regular400: TextStyle,
)

@Immutable
data class Typography(
    val textAppearance: TextAppearance = TextAppearance(
        large500 = large500,
        regular500 = regular500,
        regular400 = regular400
    )
)

internal val LocalTypography = staticCompositionLocalOf { Typography() }
