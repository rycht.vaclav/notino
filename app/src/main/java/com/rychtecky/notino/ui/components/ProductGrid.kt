package com.rychtecky.notino.ui.components

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.dp
import com.example.app_domain.model.Product
import androidx.compose.foundation.lazy.grid.items
import com.rychtecky.notino.viewmodels.NotinoViewModel

/*
* Created by Václav Rychtecký on 05/07/2023
*/
@Composable
fun ProductGrid(products: List<Product>, viewModel: NotinoViewModel, gridCells: Int = 2) {
    LazyVerticalGrid(
        columns = GridCells.Fixed(gridCells),
        contentPadding = PaddingValues(all = 20.dp)
    ) {
        items(products) { item ->
            ProductItem(product = item, viewModel = viewModel)
        }
    }
}