package com.rychtecky.notino.ui.theme

import androidx.compose.runtime.Immutable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color

@Immutable
data class Colors(
    val primaryBackground: Color = ColorsPalette.white,
    val primary: Color = ColorsPalette.inkPrimary,
    val secondary: Color = ColorsPalette.inkSecondary,
    val tertiary: Color = ColorsPalette.inkTertiary,
    val pink: Color = ColorsPalette.pink,
    val gray: Color = ColorsPalette.gray,
    val grayLight: Color = ColorsPalette.grayLight
)

val lightColors: Colors = lightColors()
val darkColors: Colors = darkColors()

fun lightColors(): Colors = Colors()
fun darkColors(): Colors =
    Colors(primaryBackground = ColorsPalette.inkPrimary, primary = ColorsPalette.white)

internal val LocalColors = staticCompositionLocalOf { lightColors() }
