package com.rychtecky.notino.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconToggleButton
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.example.app_domain.model.Product
import com.rychtecky.notino.Extensions.toCzk
import com.rychtecky.notino.R
import com.rychtecky.notino.ui.theme.NotinoTheme
import com.rychtecky.notino.viewmodels.NotinoViewModel
import timber.log.Timber

/*
* Created by Václav Rychtecký on 05/06/2023
*/
@Composable
fun ProductItem(product: Product, viewModel: NotinoViewModel) {

    Timber.d("ProductID: ${product.productId}")

    Column(
        modifier = Modifier.width(width = 164.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        IconToggleButton(
            modifier = Modifier
                .size(48.dp)
                .align(Alignment.End),
            checked = product.isFavorite,
            onCheckedChange = { viewModel.saveFavoriteAndMark(product) }) {
            Icon(
                modifier = Modifier.size(16.dp),
                imageVector = Icons.Outlined.Favorite,
                contentDescription = "",
                tint = if (product.isFavorite) NotinoTheme.colors.pink else NotinoTheme.colors.tertiary
            )
        }
        Box(
            modifier = Modifier
                .size(width = 164.dp, height = 160.dp)
                .padding(bottom = 12.dp)
                .background(color = NotinoTheme.colors.primaryBackground),
            contentAlignment = Alignment.Center
        ) {
            AsyncImage(
                model = getImageUrl(product.imageUrl),
                placeholder = painterResource(id = R.drawable.ic_launcher_foreground),
                contentScale = ContentScale.Inside,
                contentDescription = ""
            )
        }
        Text(
            modifier = Modifier.padding(bottom = 4.dp),
            textAlign = TextAlign.Center,
            text = product.brand.name,
            style = NotinoTheme.Typography.textAppearance.regular400,
            color = NotinoTheme.colors.primary
        )
        Text(
            textAlign = TextAlign.Center,
            text = product.name,
            style = NotinoTheme.Typography.textAppearance.regular500,
            color = NotinoTheme.colors.primary
        )
        Text(
            modifier = Modifier.padding(bottom = 12.dp),
            textAlign = TextAlign.Center,
            text = product.annotation,
            style = NotinoTheme.Typography.textAppearance.regular400,
            color = NotinoTheme.colors.primary
        )
        RatingBar(modifier = Modifier.padding(bottom = 12.dp), stars = product.reviewSummary.score)
        Text(
            modifier = Modifier.padding(bottom = 12.dp),
            textAlign = TextAlign.Center,
            text = product.price.value.toString() + " " + product.price.currency.toCzk(),
            style = NotinoTheme.Typography.textAppearance.regular500,
            color = NotinoTheme.colors.primary
        )
        OutlinedButton(
            border = BorderStroke(width = 2.dp, color = NotinoTheme.colors.gray),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = NotinoTheme.colors.primaryBackground
            ),
            onClick = {
                viewModel.changeInCaseStatus(product.productId)
            }) {
            Text(
                modifier = Modifier
                    .padding(horizontal = 12.dp, vertical = 8.dp),
                textAlign = TextAlign.Center,
                text = "Do košíku",
                style = NotinoTheme.Typography.textAppearance.regular500,
                color = if (product.isInCase) NotinoTheme.colors.tertiary else NotinoTheme.colors.primary
            )
        }
        Spacer(modifier = Modifier.size(12.dp))
    }
}

private fun getImageUrl(imageUrl: String): String {
    return "https://i.notino.com/detail_zoom/$imageUrl"
}