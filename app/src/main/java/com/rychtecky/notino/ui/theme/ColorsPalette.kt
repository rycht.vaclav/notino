package com.rychtecky.notino.ui.theme

import androidx.compose.ui.graphics.Color

/*
* Created by Václav Rychtecký on 05/05/2023
*/
object ColorsPalette {
    val white = Color(0xFFFFFFFF)
    val inkPrimary = Color(0xFF000000)
    val inkSecondary = Color(0xFF333333)
    val inkTertiary = Color(0xFF666666)
    val pink = Color(0xFFDC0069)
    val gray = Color(0xFFE5E5E5)
    val grayLight = Color(0xFFEEEEEE)
}