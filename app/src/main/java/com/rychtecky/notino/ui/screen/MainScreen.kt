package com.rychtecky.notino.ui.screen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import com.rychtecky.notino.ui.components.ProductGrid
import com.rychtecky.notino.ui.theme.NotinoTheme
import com.rychtecky.notino.viewmodels.NotinoViewModel

/*
* Created by Václav Rychtecký on 05/06/2023
*/
@Composable
fun MainScreen(viewModel: NotinoViewModel) {

    val products by viewModel.productList.collectAsState()

    Column(modifier = Modifier.background(NotinoTheme.colors.primaryBackground)) {
        TopAppBar(
            title = {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = "Produkty",
                    textAlign = TextAlign.Center
                )
            },
            backgroundColor = NotinoTheme.colors.primaryBackground
        )
        products?.let {
            ProductGrid(products = it, viewModel = viewModel)
        } ?: run {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                CircularProgressIndicator()
            }
        }
    }
}