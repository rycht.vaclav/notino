package com.rychtecky.notino.ui.components

import androidx.compose.foundation.layout.Row
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.StarRate
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.rychtecky.notino.ui.theme.NotinoTheme
import kotlin.math.floor

/*
* Created by Václav Rychtecký on 05/07/2023
*/
@Composable
fun RatingBar(modifier: Modifier, stars: Int) {

    Row(modifier = modifier) {
        for (i in 1..5) {
            if (i <= stars) {
                Icon(
                    imageVector = Icons.Default.StarRate,
                    contentDescription = "",
                    tint = NotinoTheme.colors.secondary
                )
            } else {
                Icon(
                    imageVector = Icons.Default.StarRate,
                    contentDescription = "",
                    tint = NotinoTheme.colors.grayLight
                )
            }
        }
    }
}

