package com.rychtecky.notino.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.ReadOnlyComposable

/*
* Created by Václav Rychtecký on 05/06/2023
*/
@Composable
fun NotinoTheme(
    colors: Colors = if (isSystemInDarkTheme()) darkColors else lightColors,
    typography: Typography = NotinoTheme.Typography,
    content: @Composable () -> Unit
) {
    CompositionLocalProvider(
        LocalColors provides colors,
        LocalTypography provides typography,
    ) {
        MaterialTheme(content = content)
    }
}

object NotinoTheme {
    val colors: Colors
        @Composable
        @ReadOnlyComposable
        get() = LocalColors.current
    val Typography: Typography
        @Composable
        @ReadOnlyComposable
        get() = LocalTypography.current
}